# Lamport signature.

Using the sha512 hash, write a program to sign and verify [Lamport
signatures][lamport]. Your program should be capable of

1. Producing a public private key pair. The public key should be in
   the file lamport.pub and private key should be in lamport.key. Use
   base16 (hexadecimal) encoding to make these files readable.

2. Given a private key and an arbitrary file it should generate the signature.

3. Given a arbitrary file, its purported signature, and the public key, it should verify
   the signature.

Use hexadecimal (base16 representation) when ever possible. Some helpful hints.

- The key would be used to sign sha512 hashes of document hence we
  need 512, (x, y) pairs.

- Choose x, y pairs randomly that are 512 bytes long. Therefore public
  key = 512 * (2 * 512) bits = 64 KByte

- Signatures would be on the 512 bit hash and hence is 512 * 512 = 32
  KB long.

[lamport]: <https://en.wikipedia.org/wiki/Lamport_signature>
